# お読みください #

## What's this? ##

* PSJ 2018年度東京大会の発表申込ファイルから抄録集とプログラムを生成するスクリプト集
* 

## 使い方 ##

### 提出された演題ファイルから演題の一覧表と発表者リストを生成する###

1. ".doc" ファイルから ".docx" ファイルへの一括変換 (外部プログラムが必要です)
    * Mac, Windows共通: [Doxillon](http://www.nchsoftware.com/documentconvert/jp/index.html)を用いるとできます。
    * Windowsのみ: [OMPM](https://technet.microsoft.com/ja-jp/library/ff453909(v=office.14).aspx) を用いるとできるそうですが、試していません。
    * Linux: 現時点で方法があるのか不明です。
2. ".docx" ファイルから markdown (".md" ファイル) への一括変換
    * [Pandoc](https://pandoc.org) を用いて行います。
    * Mac, Linux共通: **convert.sh** を用いる: Terminalから "./convert.sh" とタイプする。
        * "sample\_docx/" 内のすべての docxファイルが markdownに変換され、 "sample\_md" に格納されます。
    * Windows: 誰かバッチファイルを作ってください。pandocの実行時に "--wrap=none" オプションをつけるのを忘れないこと。
3. ".md" ファイルから演題情報のCSVファイルを作成
    * **list_papers.Rmd** を実行: ファイルがあるディレクトリでRを実行し、コマンドプロンプトで _knitr("list\_papers.Rmd")_ とタイプする (要 **knitr** パッケージ)。
        * 演題情報が _abst\_list.csv_ として生成されます。
        * 共著者も含む演者情報が _list\_authors.csv_ として生成されます。

## Files ##

* **convert.sh**
* **list_papers.Rmd**

