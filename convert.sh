#!/bin/bash

DIR_DOCX="./sample_docx" ## .docx ファイルの場所
DIR_MD="./sample_md" ## markdown ファイルを出力する場所

for FILE in `\find $DIR_DOCX -maxdepth 1 -name '*.docx'`; do
    ECHO $FILE
    BASENAME=`basename $FILE`
    echo $BASENAME
    OUTNAME=`echo $BASENAME | sed 's/\.docx/.md/'`
    echo $OUTNAME
    OUTFILE=${DIR_MD}/${OUTNAME}
    echo $OUTFILE
    pandoc -f docx -t markdown --wrap=none -o $OUTFILE $FILE
done
